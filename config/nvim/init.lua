
require('settings')
require('autocmds')
require('plugins/packer')
require('plugins/tabout-nvim')
require('plugins/comfortable-motion')
require('plugins/nvim-treesitter')
require('plugins/nvim-lspconfig')
require('plugins/nvim-autopairs')
require('plugins/nvim-tree')
require('plugins/telescope')
require('plugins/hop-nvim')
require('plugins/nvim-cmp')
require('plugins/feline')
require('compiler')
require('mappings')

-- ISSUES
-- require('plugins/shade-nvim')
-- https://www.reddit.com/r/neovim/comments/qe7c7v/set_bglightdark_on_the_fly_not_fixed_yet/
